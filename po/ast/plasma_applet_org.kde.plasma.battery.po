# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# enolp <enolp@softastur.org>, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-16 00:46+0000\n"
"PO-Revision-Date: 2021-05-06 23:52+0200\n"
"Last-Translator: enolp <enolp@softastur.org>\n"
"Language-Team: Asturian <alministradores@softastur.org>\n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.03.90\n"

#: package/contents/ui/BatteryItem.qml:105
#, kde-format
msgctxt "Placeholder is battery percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:166
#, fuzzy, kde-format
#| msgid ""
#| "This battery's health is at only %1% and should be replaced. Please "
#| "contact your hardware vendor for more details."
msgid ""
"This battery's health is at only %1% and it should be replaced. Contact the "
"manufacturer."
msgstr ""
"La vida d'esta batería ta al %1% y debería cambiase. Contauta col so "
"fabricante pa más detalles, por favor."

#: package/contents/ui/BatteryItem.qml:181
#, kde-format
msgid "Time To Full:"
msgstr "Tiempu pa completar:"

#: package/contents/ui/BatteryItem.qml:182
#, kde-format
msgid "Remaining Time:"
msgstr ""

#: package/contents/ui/BatteryItem.qml:198
#, kde-format
msgid "Battery Health:"
msgstr "Vida de la batería:"

#: package/contents/ui/BatteryItem.qml:204
#, kde-format
msgctxt "Placeholder is battery health percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:218
#, kde-format
msgid "Battery is configured to charge up to approximately %1%."
msgstr ""

#: package/contents/ui/BrightnessItem.qml:64
#, kde-format
msgctxt "Placeholder is brightness percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/CompactRepresentation.qml:84
#, kde-format
msgctxt "battery percentage below battery icon"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/logic.js:12
#, kde-format
msgid "Discharging"
msgstr "Descargando"

#: package/contents/ui/logic.js:13 package/contents/ui/main.qml:136
#, kde-format
msgid "Fully Charged"
msgstr "Cargó dafechu"

#: package/contents/ui/logic.js:14
#, kde-format
msgid "Charging"
msgstr "Cargando"

#: package/contents/ui/logic.js:16
#, kde-format
msgid "Not Charging"
msgstr "Nun ta cargando"

#: package/contents/ui/logic.js:19
#, kde-format
msgctxt "Battery is currently not present in the bay"
msgid "Not present"
msgstr "Ausente"

#: package/contents/ui/main.qml:99 package/contents/ui/main.qml:327
#, kde-format
msgid "Battery and Brightness"
msgstr "Batería y brilléu"

#: package/contents/ui/main.qml:100
#, fuzzy, kde-format
#| msgid "Display Brightness"
msgid "Brightness"
msgstr "Brilléu de la pantalla"

#: package/contents/ui/main.qml:101
#, fuzzy, kde-format
#| msgid "Battery Health:"
msgid "Battery"
msgstr "Vida de la batería:"

#: package/contents/ui/main.qml:101
#, fuzzy, kde-format
#| msgid "Enable Power Management"
msgid "Power Management"
msgstr "Activar l'aforru enerxéticu"

#: package/contents/ui/main.qml:143
#, kde-format
msgid "Battery at %1%, not Charging"
msgstr ""

#: package/contents/ui/main.qml:145
#, kde-format
msgid "Battery at %1%, plugged in but still discharging"
msgstr ""

#: package/contents/ui/main.qml:147
#, kde-format
msgid "Battery at %1%, Charging"
msgstr ""

#: package/contents/ui/main.qml:150
#, kde-format
msgid "Battery at %1%"
msgstr ""

#: package/contents/ui/main.qml:158
#, kde-format
msgid "The power supply is not powerful enough to charge the battery"
msgstr ""

#: package/contents/ui/main.qml:162
#, kde-format
msgid "No Batteries Available"
msgstr "Nun hai bateríes disponibles"

#: package/contents/ui/main.qml:168
#, kde-format
msgctxt "time until fully charged - HH:MM"
msgid "%1 until fully charged"
msgstr ""

#: package/contents/ui/main.qml:170
#, kde-format
msgctxt "remaining time left of battery usage - HH:MM"
msgid "%1 remaining"
msgstr ""

#: package/contents/ui/main.qml:173
#, kde-format
msgid "Not charging"
msgstr ""

#: package/contents/ui/main.qml:177
#, kde-format
msgid "Automatic sleep and screen locking are disabled"
msgstr ""

#: package/contents/ui/main.qml:276
#, kde-format
msgid "The battery applet has enabled system-wide inhibition"
msgstr "L'applet de batería activó la inhibición en tol sistema"

#: package/contents/ui/main.qml:330
#, kde-format
msgid "Failed to activate %1 mode"
msgstr ""

#: package/contents/ui/main.qml:342
#, fuzzy, kde-format
#| msgid "&Show Energy Information..."
msgid "&Show Energy Information…"
msgstr "&Amosar la información enerxética"

#: package/contents/ui/main.qml:344
#, kde-format
msgid "Show Battery Percentage on Icon"
msgstr ""

#: package/contents/ui/main.qml:350
#, fuzzy, kde-format
#| msgid "Configure Power Saving..."
msgid "&Configure Energy Saving…"
msgstr "Configurar l'aforru enerxéticu…"

#: package/contents/ui/PopupDialog.qml:117
#, kde-format
msgid "Display Brightness"
msgstr "Brilléu de la pantalla"

#: package/contents/ui/PopupDialog.qml:146
#, kde-format
msgid "Keyboard Brightness"
msgstr "Brilléu del tecláu"

#: package/contents/ui/PowerManagementItem.qml:37
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid "Manually block sleep and screen locking"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:70
#, fuzzy, kde-format
#| msgctxt "Minimize the length of this string as much as possible"
#| msgid ""
#| "Your notebook is configured not to sleep when closing the lid while an "
#| "external monitor is connected."
msgctxt "Minimize the length of this string as much as possible"
msgid ""
"Your laptop is configured not to sleep when closing the lid while an "
"external monitor is connected."
msgstr ""
"El portátil ta configuráu pa nun se suspender al baxar la tapa mentanto tea "
"conectáu a una pantalla esterna."

#: package/contents/ui/PowerManagementItem.qml:83
#, kde-format
msgid "%1 application is currently blocking sleep and screen locking:"
msgid_plural "%1 applications are currently blocking sleep and screen locking:"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/PowerManagementItem.qml:103
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (%2)"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:104
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (unknown reason)"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:106
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerManagementItem.qml:107
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: unknown reason"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:34
#, kde-format
msgid "Power Save"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:38
#, kde-format
msgid "Balanced"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:42
#, kde-format
msgid "Performance"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:59
#, kde-format
msgid "Power Profile"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:191
#, kde-format
msgid ""
"Performance mode has been disabled to reduce heat generation because the "
"computer has detected that it may be sitting on your lap."
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:193
#, kde-format
msgid ""
"Performance mode is unavailable because the computer is running too hot."
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:195
#, fuzzy, kde-format
#| msgid "Power management is disabled"
msgid "Performance mode is unavailable."
msgstr "La xestión enerxética ta desactivada"

#: package/contents/ui/PowerProfileItem.qml:208
#, kde-format
msgid ""
"Performance may be lowered to reduce heat generation because the computer "
"has detected that it may be sitting on your lap."
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:210
#, kde-format
msgid "Performance may be reduced because the computer is running too hot."
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:212
#, fuzzy, kde-format
#| msgid "Power management is disabled"
msgid "Performance may be reduced."
msgstr "La xestión enerxética ta desactivada"

#: package/contents/ui/PowerProfileItem.qml:223
#, kde-format
msgid "One application has requested activating %2:"
msgid_plural "%1 applications have requested activating %2:"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/PowerProfileItem.qml:241
#, fuzzy, kde-format
#| msgctxt "Application name: reason for preventing sleep and screen locking"
#| msgid "%1: %2"
msgctxt ""
"%1 is the name of the application, %2 is the reason provided by it for "
"activating performance mode"
msgid "%1: %2"
msgstr "%1: %2"

#~ msgid "General"
#~ msgstr "Xeneral"

#~ msgctxt "short symbol to signal there is no battery currently available"
#~ msgid "-"
#~ msgstr "-"

#~ msgid ""
#~ "Disabling power management will prevent your screen and computer from "
#~ "turning off automatically.\n"
#~ "\n"
#~ "Most applications will automatically suppress power management when they "
#~ "don't want to have you interrupted."
#~ msgstr ""
#~ "La desactivación de la xestión enerxética va evitar que la pantalla y "
#~ "l'ordenador s'apaguen automáticamente.\n"
#~ "\n"
#~ "La mayoría d'aplicaciones van suspender automáticamente la xestión "
#~ "enerxética cuando nun quieran que tengas interrupciones."

#~ msgctxt "Some Application and n others are currently suppressing PM"
#~ msgid ""
#~ "%2 and %1 other application are currently suppressing power management."
#~ msgid_plural ""
#~ "%2 and %1 other applications are currently suppressing power management."
#~ msgstr[0] "%2 y %1 aplicación más tán suspendiendo la xestión enerxética."
#~ msgstr[1] "%2 y %1 aplicaciones más tán suspendiendo la xestión enerxética."

#~ msgctxt "Some Application is suppressing PM"
#~ msgid "%1 is currently suppressing power management."
#~ msgstr "%1 ta suspendiendo la xestión enerxética."

#~ msgctxt "Some Application is suppressing PM: Reason provided by the app"
#~ msgid "%1 is currently suppressing power management: %2"
#~ msgstr "%1 ta suspendiendo la xestión enerxética: %2"

#~ msgctxt "Used for measurement"
#~ msgid "100%"
#~ msgstr "100%"

#~ msgid "%1% Battery Remaining"
#~ msgstr "%1% batería restante"
