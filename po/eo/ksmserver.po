# translation of ksmserver.po to Esperanto
# Copyright (C) 2000, 2007, 2008 Free Software Foundation, Inc.
# Wolfram Diestel <wolfram@steloj.de>, 2000.
# Cindy McKee <cfmckee@gmail.com>, 2007, 2008.
#
# Minuskloj: ĉ ĝ ĵ ĥ ŝ ŭ   Majuskloj: Ĉ Ĝ Ĵ Ĥ Ŝ Ŭ
#
msgid ""
msgstr ""
"Project-Id-Version: ksmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-04-22 00:47+0000\n"
"PO-Revision-Date: 2008-01-02 08:02-0600\n"
"Last-Translator: Cindy McKee <cfmckee@gmail.com>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: logout.cpp:344
#, kde-format
msgid "Logout canceled by '%1'"
msgstr "'%1' nuligis la elsaluton"

#: main.cpp:117
#, kde-format
msgid "$HOME not set!"
msgstr ""

#: main.cpp:121 main.cpp:127
#, kde-format
msgid "$HOME directory (%1) does not exist."
msgstr ""

#: main.cpp:123
#, kde-format
msgid "No write access to $HOME directory (%1)."
msgstr ""

#: main.cpp:129
#, kde-format
msgid "No read access to $HOME directory (%1)."
msgstr ""

#: main.cpp:133
#, kde-format
msgid "$HOME directory (%1) is out of disk space."
msgstr ""

#: main.cpp:136
#, kde-format
msgid "Writing to the $HOME directory (%2) failed with the error '%1'"
msgstr ""

#: main.cpp:149 main.cpp:184
#, kde-format
msgid "No write access to '%1'."
msgstr ""

#: main.cpp:151 main.cpp:186
#, kde-format
msgid "No read access to '%1'."
msgstr ""

#: main.cpp:159 main.cpp:172
#, kde-format
msgid "Temp directory (%1) is out of disk space."
msgstr ""

#: main.cpp:162 main.cpp:175
#, kde-format
msgid ""
"Writing to the temp directory (%2) failed with\n"
"    the error '%1'"
msgstr ""

#: main.cpp:190
#, kde-format
msgid ""
"The following installation problem was detected\n"
"while trying to start Plasma:"
msgstr ""

#: main.cpp:193
#, kde-format
msgid ""
"\n"
"\n"
"Plasma is unable to start.\n"
msgstr ""

#: main.cpp:200
#, kde-format
msgid "Plasma Workspace installation problem!"
msgstr ""

#: main.cpp:236
#, fuzzy, kde-format
#| msgid ""
#| "The reliable KDE session manager that talks the standard X11R6 \n"
#| "session management protocol (XSMP)."
msgid ""
"The reliable Plasma session manager that talks the standard X11R6 \n"
"session management protocol (XSMP)."
msgstr ""
"La fidinda KDE-seancadministrilo kiu parolas la norman X11R6-"
"seancadministran protokolon XSMP."

#: main.cpp:240
#, kde-format
msgid "Restores the saved user session if available"
msgstr "Restarigas la antaŭan uzantoseancon, se ekzistas"

#: main.cpp:243
#, kde-format
msgid "Also allow remote connections"
msgstr "Permesi ankaŭ forajn konektojn"

#: main.cpp:246
#, kde-format
msgid "Starts the session in locked mode"
msgstr ""

#: main.cpp:250
#, kde-format
msgid ""
"Starts without lock screen support. Only needed if other component provides "
"the lock screen."
msgstr ""

#: server.cpp:873
#, fuzzy, kde-format
#| msgid "The KDE Session Manager"
msgid "Session Management"
msgstr "La KDE-seanco-administrilo"

#: server.cpp:876
#, fuzzy, kde-format
#| msgid "Logout"
msgid "Log Out"
msgstr "Elsaluto"

#: server.cpp:881
#, kde-format
msgid "Log Out Without Confirmation"
msgstr "Elsaluti sen konfirmo"

#: server.cpp:886
#, kde-format
msgid "Halt Without Confirmation"
msgstr "Ĉesi sen konfirmo"

#: server.cpp:891
#, kde-format
msgid "Reboot Without Confirmation"
msgstr "Reŝargi sen konfirmo"

#~ msgid ""
#~ "Starts <wm> in case no other window manager is \n"
#~ "participating in the session. Default is 'kwin'"
#~ msgstr ""
#~ "Lanĉas <wm> se neniu alia fenestroadministrilo partoprenas\n"
#~ "en la seanco. La defaŭlta estas 'kwin'"

#, fuzzy
#~| msgid "Logout"
#~ msgid "Logout"
#~ msgstr "Elsaluto"

#, fuzzy
#~| msgid "Turn Off Computer"
#~ msgid "Sleeping in 1 second"
#~ msgid_plural "Sleeping in %1 seconds"
#~ msgstr[0] "Malŝalti la komputilon"
#~ msgstr[1] "Malŝalti la komputilon"

#, fuzzy
#~| msgid "Turn Off Computer"
#~ msgid "Logging out in 1 second."
#~ msgid_plural "Logging out in %1 seconds."
#~ msgstr[0] "Malŝalti la komputilon"
#~ msgstr[1] "Malŝalti la komputilon"

#, fuzzy
#~| msgid "Turn Off Computer"
#~ msgid "Turning off computer in 1 second."
#~ msgid_plural "Turning off computer in %1 seconds."
#~ msgstr[0] "Malŝalti la komputilon"
#~ msgstr[1] "Malŝalti la komputilon"

#, fuzzy
#~| msgid "Turn Off Computer"
#~ msgid "Restarting computer in 1 second."
#~ msgid_plural "Restarting computer in %1 seconds."
#~ msgstr[0] "Malŝalti la komputilon"
#~ msgstr[1] "Malŝalti la komputilon"

#, fuzzy
#~| msgid "Turn Off Computer"
#~ msgid "Turn Off Computer"
#~ msgstr "Malŝalti la komputilon"

#, fuzzy
#~| msgid "Restart Computer"
#~ msgid "Restart Computer"
#~ msgstr "Relanĉi la komputilon"

#, fuzzy
#~| msgid "Cancel"
#~ msgid "Cancel"
#~ msgstr "Rezigni"

#, fuzzy
#~| msgid "Standby"
#~ msgid "&Standby"
#~ msgstr "Suspendi"

#, fuzzy
#~| msgid "Suspend to RAM"
#~ msgid "Suspend to &RAM"
#~ msgstr "Suspendi al RAM"

#, fuzzy
#~| msgid "Suspend to Disk"
#~ msgid "Suspend to &Disk"
#~ msgstr "Suspendi al disko"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Wolfram Diestel,Steffen Pietsch"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "wolfram@steloj.de,Steffen.Pietsch@BerlinOnline.de"

#~ msgid "(C) 2000, The KDE Developers"
#~ msgstr "(C) 2000, The KDE Developers"

#~ msgid "Matthias Ettrich"
#~ msgstr "Matthias Ettrich"

#~ msgid "Luboš Luňák"
#~ msgstr "Luboš Luňák"

#~ msgid "Maintainer"
#~ msgstr "Prizorganto"

#~ msgctxt "current option in boot loader"
#~ msgid " (current)"
#~ msgstr " (nuna)"

#~ msgctxt "@label In corner of the logout dialog"
#~ msgid "KDE <numid>%1.%2.%3</numid>"
#~ msgstr "KDE <numid>%1.%2.%3</numid>"

#~ msgctxt "@label In corner of the logout dialog"
#~ msgid "KDE <numid>%1.%2</numid>"
#~ msgstr "KDE <numid>%1.%2</numid>"

#~ msgid "End Session for %1"
#~ msgstr "Fini seancon por %1"

#~ msgid "End Session for %1 (%2)"
#~ msgstr "Fini seancon por %1 (%2)"
