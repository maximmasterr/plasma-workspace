# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Kristóf Kiszel <ulysses@kubuntu.org>, 2014, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-09 00:50+0000\n"
"PO-Revision-Date: 2020-11-16 11:31+0100\n"
"Last-Translator: Kristóf Kiszel <kiszel.kristof@gmail.com>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.03.70\n"

#: contents/config/config.qml:12
#, kde-format
msgid "General"
msgstr ""

#: contents/ui/AlbumArtStackView.qml:161
#, kde-format
msgid "No title"
msgstr ""

#: contents/ui/AlbumArtStackView.qml:161 contents/ui/main.qml:80
#, kde-format
msgid "No media playing"
msgstr "Nincs médialejátszás"

#: contents/ui/ConfigGeneral.qml:22
#, kde-format
msgid "Volume step:"
msgstr ""

#: contents/ui/ExpandedRepresentation.qml:363
#: contents/ui/ExpandedRepresentation.qml:484
#, kde-format
msgctxt "Remaining time for song e.g -5:42"
msgid "-%1"
msgstr "-%1"

#: contents/ui/ExpandedRepresentation.qml:513
#, kde-format
msgid "Shuffle"
msgstr "Véletlen sorrend"

#: contents/ui/ExpandedRepresentation.qml:539 contents/ui/main.qml:98
#, kde-format
msgctxt "Play previous track"
msgid "Previous Track"
msgstr "Előző szám"

#: contents/ui/ExpandedRepresentation.qml:561 contents/ui/main.qml:106
#, kde-format
msgctxt "Pause playback"
msgid "Pause"
msgstr "Szünet"

#: contents/ui/ExpandedRepresentation.qml:561 contents/ui/main.qml:111
#, kde-format
msgctxt "Start playback"
msgid "Play"
msgstr "Lejátszás"

#: contents/ui/ExpandedRepresentation.qml:579 contents/ui/main.qml:117
#, kde-format
msgctxt "Play next track"
msgid "Next Track"
msgstr "Következő szám"

#: contents/ui/ExpandedRepresentation.qml:602
#, kde-format
msgid "Repeat Track"
msgstr "Szám ismétlése"

#: contents/ui/ExpandedRepresentation.qml:602
#, kde-format
msgid "Repeat"
msgstr "Ismétlés"

#: contents/ui/main.qml:94
#, kde-format
msgctxt "Open player window or bring it to the front if already open"
msgid "Open"
msgstr "Megnyitás"

#: contents/ui/main.qml:123
#, kde-format
msgctxt "Stop playback"
msgid "Stop"
msgstr "Leállítás"

#: contents/ui/main.qml:131
#, kde-format
msgctxt "Quit player"
msgid "Quit"
msgstr "Kilépés"

#: contents/ui/main.qml:241
#, kde-format
msgid "Choose player automatically"
msgstr "Lejátszó automatikus kiválasztása"

#: contents/ui/main.qml:278
#, kde-format
msgctxt "by Artist (player name)"
msgid "by %1 (%2)"
msgstr "előadó: %1 (%2)"

#: contents/ui/main.qml:289
#, kde-format
msgctxt "by Artist (paused, player name)"
msgid "by %1 (paused, %2)"
msgstr "előadó: %1 (szüneteltetve, %2)"

#: contents/ui/main.qml:289
#, kde-format
msgctxt "Paused (player name)"
msgid "Paused (%1)"
msgstr "Szüneteltetve (%1)"

#~ msgctxt "artist – track"
#~ msgid "%1 – %2"
#~ msgstr "%1 – %2"

#~ msgctxt "Artist of the song"
#~ msgid "by %1"
#~ msgstr "előadó: %1"
