# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Sveinn í Felli <sv1@fellsnet.is>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-18 00:47+0000\n"
"PO-Revision-Date: 2022-08-24 15:34+0000\n"
"Last-Translator: Sveinn í Felli <sv1@fellsnet.is>\n"
"Language-Team: Icelandic\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Almennt"

#: package/contents/config/config.qml:18
#, kde-format
msgid "Entries"
msgstr "Færslur"

#: package/contents/ui/ConfigEntries.qml:31
#, kde-format
msgid "Always show all entries"
msgstr "Alltaf birta allar færslur"

#: package/contents/ui/ConfigEntries.qml:37
#, kde-format
msgid "Application Status"
msgstr "Staða forrits"

#: package/contents/ui/ConfigEntries.qml:39
#, kde-format
msgid "Communications"
msgstr "Samskipti"

#: package/contents/ui/ConfigEntries.qml:41
#, kde-format
msgid "System Services"
msgstr "Kerfisþjónustur"

#: package/contents/ui/ConfigEntries.qml:43
#, kde-format
msgid "Hardware Control"
msgstr "Vélbúnaðarstjórnun"

#: package/contents/ui/ConfigEntries.qml:46
#, kde-format
msgid "Miscellaneous"
msgstr "Ýmislegt"

#: package/contents/ui/ConfigEntries.qml:80
#, kde-format
msgctxt "Name of the system tray entry"
msgid "Entry"
msgstr "Færsla"

#: package/contents/ui/ConfigEntries.qml:85
#, kde-format
msgid "Visibility"
msgstr "Sýnileiki"

#: package/contents/ui/ConfigEntries.qml:91
#, kde-format
msgid "Keyboard Shortcut"
msgstr "Flýtilykill"

#: package/contents/ui/ConfigEntries.qml:222
#, kde-format
msgid "Shown when relevant"
msgstr "Sýna þegar skiptir máli"

#: package/contents/ui/ConfigEntries.qml:223
#, kde-format
msgid "Always shown"
msgstr "Alltaf sýnt"

#: package/contents/ui/ConfigEntries.qml:224
#, kde-format
msgid "Always hidden"
msgstr "Alltaf falið"

#: package/contents/ui/ConfigEntries.qml:225
#, kde-format
msgid "Disabled"
msgstr "Óvirkt"

#: package/contents/ui/ConfigGeneral.qml:23
#, kde-format
msgctxt "The arrangement of system tray icons in the Panel"
msgid "Panel icon size:"
msgstr "Táknmyndastærð á spjaldi:"

#: package/contents/ui/ConfigGeneral.qml:25
#, kde-format
msgid "Small"
msgstr "Lítil"

#: package/contents/ui/ConfigGeneral.qml:32
#, kde-format
msgid "Scale with Panel height"
msgstr "Kvarða með hæð spjalds"

#: package/contents/ui/ConfigGeneral.qml:33
#, kde-format
msgid "Scale with Panel width"
msgstr "Kvarða með breidd spjalds"

#: package/contents/ui/ConfigGeneral.qml:39
#, kde-format
msgid "Automatically enabled when in Touch Mode"
msgstr "Sjálfvirkt virkjað þegar verið er í snertiham"

#: package/contents/ui/ConfigGeneral.qml:48
#, kde-format
msgctxt "@label:listbox The spacing between system tray icons in the Panel"
msgid "Panel icon spacing:"
msgstr "Bil milli táknmynda á spjaldi:"

#: package/contents/ui/ConfigGeneral.qml:51
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Small"
msgstr "Lítil"

#: package/contents/ui/ConfigGeneral.qml:55
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Normal"
msgstr "Venjuleg"

#: package/contents/ui/ConfigGeneral.qml:59
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Large"
msgstr "Stór"

#: package/contents/ui/ConfigGeneral.qml:82
#, kde-format
msgctxt "@info:usagetip under a combobox when Touch Mode is on"
msgid "Automatically set to Large when in Touch Mode"
msgstr "Sjálfvirkt stillt á stórar þegar verið er í snertiham"

#: package/contents/ui/ExpandedRepresentation.qml:64
#, kde-format
msgctxt "@action:button"
msgid "Go Back"
msgstr "Fara til baka"

#: package/contents/ui/ExpandedRepresentation.qml:77
#, kde-format
msgid "Status and Notifications"
msgstr "Staða og tilkynningar"

#: package/contents/ui/ExpandedRepresentation.qml:93
#, kde-format
msgid "More actions"
msgstr "Fleiri aðgerðir"

#: package/contents/ui/ExpandedRepresentation.qml:182
#, kde-format
msgid "Keep Open"
msgstr "Halda opnu"

#: package/contents/ui/ExpanderArrow.qml:22
#, kde-format
msgid "Expand System Tray"
msgstr "Útvíkka kerfisbakka"

#: package/contents/ui/ExpanderArrow.qml:23
#, kde-format
msgid "Show all the items in the system tray in a popup"
msgstr "Birta öll atriði úr kerfisbakka í sprettglugga"

#: package/contents/ui/ExpanderArrow.qml:36
#, kde-format
msgid "Close popup"
msgstr "Loka sprettglugga"

#: package/contents/ui/ExpanderArrow.qml:36
#, kde-format
msgid "Show hidden icons"
msgstr "Birta faldar táknmyndir"

#: systemtraymodel.cpp:123
#, kde-format
msgctxt ""
"Suffix added to the applet name if the applet is autoloaded via DBus "
"activation"
msgid "%1 (Automatic load)"
msgstr "%1 (sjálfvirk hleðsla)"
