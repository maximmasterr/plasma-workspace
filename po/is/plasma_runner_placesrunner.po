# translation of plasma_runner_placesrunner.po to Icelandic
# Copyright (C) 2009 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sveinn í Felli <sveinki@nett.is>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_placesrunner\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-01-27 00:45+0000\n"
"PO-Revision-Date: 2009-11-08 18:37+0000\n"
"Last-Translator: Sveinn í Felli <sveinki@nett.is>\n"
"Language-Team: Icelandic <kde-isl@molar.is>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"\n"
"\n"

#: placesrunner.cpp:29 placesrunner.cpp:83
#, kde-format
msgid "places"
msgstr "staðir"

#: placesrunner.cpp:29
#, kde-format
msgid "Lists all file manager locations"
msgstr "Gera lista með öllum staðsetningum í skráastjóra"

#: placesrunner.cpp:31
#, kde-format
msgid "Finds file manager locations that match :q:"
msgstr "Finnur staði í skráastjóra sem samsvara :q:"
