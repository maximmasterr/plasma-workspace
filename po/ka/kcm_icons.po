# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-15 00:48+0000\n"
"PO-Revision-Date: 2022-09-02 07:17+0200\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.1.1\n"

#: iconsizecategorymodel.cpp:14
#, kde-format
msgid "Toolbar"
msgstr "ხელსაწყოთა ზოლი"

#: iconsizecategorymodel.cpp:15
#, kde-format
msgid "Main Toolbar"
msgstr "ხელსაწყოთა მთავარი ზოლი"

#: iconsizecategorymodel.cpp:16
#, kde-format
msgid "Small Icons"
msgstr "პატარა ხატულები"

#: iconsizecategorymodel.cpp:17
#, kde-format
msgid "Panel"
msgstr "ზოლი"

#: iconsizecategorymodel.cpp:18
#, kde-format
msgid "Dialogs"
msgstr "დიალოგები"

#. i18n: ectx: label, entry (Theme), group (Icons)
#: iconssettingsbase.kcfg:9
#, kde-format
msgid "Name of the current icon theme"
msgstr "ხატულების მიმდინარე თემის სახელი"

#. i18n: ectx: label, entry (desktopSize), group (DesktopIcons)
#: iconssettingsbase.kcfg:15
#, kde-format
msgid "Desktop icons size"
msgstr "სამუშაო მაგიდის ხატულების ზომა"

#. i18n: ectx: label, entry (toolbarSize), group (ToolbarIcons)
#: iconssettingsbase.kcfg:21
#, kde-format
msgid "Toolbar icons size"
msgstr "ხელსაწყოების ზოლის ხატულების ზომა"

#. i18n: ectx: label, entry (mainToolbarSize), group (MainToolbarIcons)
#: iconssettingsbase.kcfg:27
#, kde-format
msgid "Main toolbar icons size"
msgstr "ხელსაწყოების მთავარი ზოლის ხატულების ზომა"

#. i18n: ectx: label, entry (smallSize), group (SmallIcons)
#: iconssettingsbase.kcfg:33
#, kde-format
msgid "Small icons size"
msgstr "პატარა ხატულების ზომა"

#. i18n: ectx: label, entry (panelSize), group (PanelIcons)
#: iconssettingsbase.kcfg:39
#, kde-format
msgid "Panel icons size"
msgstr "პანელის ხატულების ზომა"

#. i18n: ectx: label, entry (dialogSize), group (DialogIcons)
#: iconssettingsbase.kcfg:45
#, kde-format
msgid "Dialog icons size"
msgstr "ფანჯრის ხატულების ზომა"

#: main.cpp:182
#, kde-format
msgid "Unable to create a temporary file."
msgstr "დროებითი ფაილის შექმნის შეცდომა."

#: main.cpp:193
#, kde-format
msgid "Unable to download the icon theme archive: %1"
msgstr "ხატულების თემის არქივის გადმოწერის შეცდომა: %1"

#: main.cpp:207
#, kde-format
msgid "The file is not a valid icon theme archive."
msgstr "ფაილი ხატულების თემის სწორ არქივს არ წარმოადგენს."

#: main.cpp:212
#, kde-format
msgid ""
"A problem occurred during the installation process; however, most of the "
"themes in the archive have been installed"
msgstr "დაყენების პროცესის შეცდომა. მაგრამ არქივის ძირითადი თემები დაყენებულია"

#: main.cpp:216
#, kde-format
msgid "Theme installed successfully."
msgstr "თემა წარმატებით დადგა."

#: main.cpp:260
#, kde-format
msgid "Installing icon themes…"
msgstr "ხატულის თემების დაყენება…"

#: main.cpp:270
#, kde-format
msgid "Installing %1 theme…"
msgstr "თემის დაყენება: %1 …"

#: package/contents/ui/IconSizePopup.qml:95
#, kde-format
msgctxt "@label:slider"
msgid "Size:"
msgstr ""

#: package/contents/ui/main.qml:22
#, kde-format
msgid "This module allows you to choose the icons for your desktop."
msgstr "ეს მოდული საშუალებას გაძლევთ აირჩიოთ თქვენი სამუშაო მაგიდის ხატულები."

#: package/contents/ui/main.qml:148
#, kde-format
msgid "Remove Icon Theme"
msgstr "ხატულების თემის წაშლა"

#: package/contents/ui/main.qml:155
#, kde-format
msgid "Restore Icon Theme"
msgstr "ხატულების თემის აღდგენა"

#: package/contents/ui/main.qml:229
#, kde-format
msgid "Configure Icon Sizes"
msgstr "ხატულების ზომის მორგება"

#: package/contents/ui/main.qml:246
#, kde-format
msgid "Install from File…"
msgstr "ფაილიდან დაყენება…"

#: package/contents/ui/main.qml:251
#, kde-format
msgid "Get New Icons…"
msgstr "ახალი ხატულების მიღება…"

#: package/contents/ui/main.qml:273
#, fuzzy, kde-format
#| msgid "Configure Icon Sizes"
msgctxt "@title:window"
msgid "Configure Icon Sizes"
msgstr "ხატულების ზომის მორგება"

#: package/contents/ui/main.qml:285
#, kde-format
msgid "Open Theme"
msgstr "თემის გახსნა"

#: package/contents/ui/main.qml:287
#, kde-format
msgid "Theme Files (*.tar.gz *.tar.bz2)"
msgstr "თემის ფაილები (*.tar.gz *.tar.bz2)"
