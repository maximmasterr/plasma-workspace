# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sergiu Bivol <sergiu@cip.md>, 2015, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-22 00:47+0000\n"
"PO-Revision-Date: 2020-09-08 19:16+0100\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 19.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Sergiu Bivol"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sergiu@cip.md"

#: main.cpp:57 view.cpp:46
#, kde-format
msgid "KRunner"
msgstr "KRunner"

#: main.cpp:57
#, kde-format
msgid "Run Command interface"
msgstr "Interfață de executare a comenzilor"

#: main.cpp:62
#, kde-format
msgid "Use the clipboard contents as query for KRunner"
msgstr "Folosește conținutul clipboard-ului ca interogare pentru KRunner"

#: main.cpp:63
#, kde-format
msgid "Start KRunner in the background, don't show it."
msgstr "Pornește KRunner în fundal, nu-l arăta."

#: main.cpp:64
#, kde-format
msgid "Replace an existing instance"
msgstr "Înlocuiește o instanță existentă"

#: main.cpp:69
#, kde-format
msgid "The query to run, only used if -c is not provided"
msgstr "Interogarea de rulat, folosită numai dacă -c nu e furnizat"

#: qml/RunCommand.qml:75
#, kde-format
msgid "Configure"
msgstr ""

#: qml/RunCommand.qml:76
#, kde-format
msgid "Configure KRunner Behavior"
msgstr ""

#: qml/RunCommand.qml:79
#, kde-format
msgid "Configure KRunner…"
msgstr ""

#: qml/RunCommand.qml:91
#, kde-format
msgctxt "Textfield placeholder text, query specific KRunner plugin"
msgid "Search '%1'…"
msgstr ""

#: qml/RunCommand.qml:92
#, kde-format
msgctxt "Textfield placeholder text"
msgid "Search…"
msgstr ""

#: qml/RunCommand.qml:228 qml/RunCommand.qml:229 qml/RunCommand.qml:231
#, kde-format
msgid "Show Usage Help"
msgstr ""

#: qml/RunCommand.qml:239
#, kde-format
msgid "Pin"
msgstr ""

#: qml/RunCommand.qml:240
#, kde-format
msgid "Pin Search"
msgstr ""

#: qml/RunCommand.qml:242
#, kde-format
msgid "Keep Open"
msgstr ""

#: qml/RunCommand.qml:320 qml/RunCommand.qml:325
#, kde-format
msgid "Recent Queries"
msgstr ""

#: qml/RunCommand.qml:323
#, kde-format
msgid "Remove"
msgstr ""

#~ msgid "krunner"
#~ msgstr "krunner"

#~ msgid "Run Command on clipboard contents"
#~ msgstr "Execută comanda asupra conținutului din clipboard"

#~ msgid "Run Command"
#~ msgstr "Execută comanda"

#, fuzzy
#~| msgid "Run Command"
#~ msgctxt "Name for krunner shortcuts category"
#~ msgid "Run Command"
#~ msgstr "Execută comanda"
