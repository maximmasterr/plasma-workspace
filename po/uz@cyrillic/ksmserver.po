# translation of ksmserver.po to Uzbek
# Copyright (C) 2003, 2004, 2005 Free Software Foundation, Inc.
#
# Mashrab Kuvatov <kmashrab@uni-bremen.de>, 2003, 2004, 2005, 2009.
msgid ""
msgstr ""
"Project-Id-Version: ksmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-04-22 00:47+0000\n"
"PO-Revision-Date: 2009-10-16 00:04+0200\n"
"Last-Translator: Mashrab Kuvatov <kmashrab@uni-bremen.de>\n"
"Language-Team: Uzbek <floss-uz-l10n@googlegroups.com>\n"
"Language: uz@cyrillic\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: logout.cpp:344
#, kde-format
msgid "Logout canceled by '%1'"
msgstr "Тизимдан чиқиш '%1' томонидан бекор қилинди"

#: main.cpp:117
#, kde-format
msgid "$HOME not set!"
msgstr ""

#: main.cpp:121 main.cpp:127
#, kde-format
msgid "$HOME directory (%1) does not exist."
msgstr ""

#: main.cpp:123
#, kde-format
msgid "No write access to $HOME directory (%1)."
msgstr ""

#: main.cpp:129
#, kde-format
msgid "No read access to $HOME directory (%1)."
msgstr ""

#: main.cpp:133
#, kde-format
msgid "$HOME directory (%1) is out of disk space."
msgstr ""

#: main.cpp:136
#, kde-format
msgid "Writing to the $HOME directory (%2) failed with the error '%1'"
msgstr ""

#: main.cpp:149 main.cpp:184
#, kde-format
msgid "No write access to '%1'."
msgstr ""

#: main.cpp:151 main.cpp:186
#, kde-format
msgid "No read access to '%1'."
msgstr ""

#: main.cpp:159 main.cpp:172
#, kde-format
msgid "Temp directory (%1) is out of disk space."
msgstr ""

#: main.cpp:162 main.cpp:175
#, kde-format
msgid ""
"Writing to the temp directory (%2) failed with\n"
"    the error '%1'"
msgstr ""

#: main.cpp:190
#, kde-format
msgid ""
"The following installation problem was detected\n"
"while trying to start Plasma:"
msgstr ""

#: main.cpp:193
#, kde-format
msgid ""
"\n"
"\n"
"Plasma is unable to start.\n"
msgstr ""

#: main.cpp:200
#, kde-format
msgid "Plasma Workspace installation problem!"
msgstr ""

#: main.cpp:236
#, kde-format
msgid ""
"The reliable Plasma session manager that talks the standard X11R6 \n"
"session management protocol (XSMP)."
msgstr ""

#: main.cpp:240
#, kde-format
msgid "Restores the saved user session if available"
msgstr "Агар иложи бўлса фойдаланувчининг сақланган сеансини қайта тиклайди"

#: main.cpp:243
#, kde-format
msgid "Also allow remote connections"
msgstr "Масофадан уланишга ҳам рухсат бериш"

#: main.cpp:246
#, kde-format
msgid "Starts the session in locked mode"
msgstr ""

#: main.cpp:250
#, kde-format
msgid ""
"Starts without lock screen support. Only needed if other component provides "
"the lock screen."
msgstr ""

#: server.cpp:873
#, fuzzy, kde-format
#| msgid "The KDE Session Manager"
msgid "Session Management"
msgstr "KDE сеанс бошқарувчиси"

#: server.cpp:876
#, fuzzy, kde-format
#| msgid "&Logout"
msgid "Log Out"
msgstr "&Чиқиш"

#: server.cpp:881
#, kde-format
msgid "Log Out Without Confirmation"
msgstr ""

#: server.cpp:886
#, kde-format
msgid "Halt Without Confirmation"
msgstr ""

#: server.cpp:891
#, kde-format
msgid "Reboot Without Confirmation"
msgstr ""

#, fuzzy
#~| msgid "&Logout"
#~ msgid "Logout"
#~ msgstr "&Чиқиш"

#, fuzzy
#~| msgid "Logging out in 1 second."
#~| msgid_plural "Logging out in %1 seconds."
#~ msgid "Sleeping in 1 second"
#~ msgid_plural "Sleeping in %1 seconds"
#~ msgstr[0] "Сеанс %1 сониядан сўнг охирига етади."

#~ msgid "Logging out in 1 second."
#~ msgid_plural "Logging out in %1 seconds."
#~ msgstr[0] "Сеанс %1 сониядан сўнг охирига етади."

#~ msgid "Turning off computer in 1 second."
#~ msgid_plural "Turning off computer in %1 seconds."
#~ msgstr[0] "Компьютер %1 сониядан сўнг ўчирилади"

#~ msgid "Restarting computer in 1 second."
#~ msgid_plural "Restarting computer in %1 seconds."
#~ msgstr[0] "Компьютер %1 сониядан сўнг ўчириб-ёқилади"

#, fuzzy
#~| msgid "&Turn Off Computer"
#~ msgid "Turn Off Computer"
#~ msgstr "&Компьютерни ўчириш"

#, fuzzy
#~| msgid "&Restart Computer"
#~ msgid "Restart Computer"
#~ msgstr "К&омпьютерни ўчириб-ёқиш"

#~ msgctxt "default option in boot loader"
#~ msgid " (default)"
#~ msgstr " (андоза)"

#, fuzzy
#~| msgid "&Cancel"
#~ msgid "Cancel"
#~ msgstr "&Бекор қилиш"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Машраб Қуватов"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "kmashrab@uni-bremen.de"

#~ msgid "(C) 2000, The KDE Developers"
#~ msgstr "(C) -2000, KDE тузувчилари"

#~ msgid "Maintainer"
#~ msgstr "Таъминловчи"

#~ msgctxt "current option in boot loader"
#~ msgid " (current)"
#~ msgstr " (амалдаги)"

#, fuzzy
#~ msgid "End Session for %1"
#~ msgstr "Фойдаланувчи \"%1\" учун сеанс якуни"

#, fuzzy
#~ msgid "End Session for %1 (%2)"
#~ msgstr "Фойдаланувчи \"%1\" учун сеанс якуни"

#, fuzzy
#~ msgid "End Current Session"
#~ msgstr "&Жорий сеансни тугатиш"
