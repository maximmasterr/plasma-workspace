# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Franklin, 2014, 2015.
# Franklin Weng <franklin@goodhorse.idv.tw>, 2015.
# pan93412 <pan93412@gmail.com>, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-09 00:47+0000\n"
"PO-Revision-Date: 2019-01-04 23:25+0800\n"
"Last-Translator: pan93412 <pan93412@gmail.com>\n"
"Language-Team: Chinese <zh-l10n@lists.linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 18.12.0\n"

#: contents/ui/BarcodePage.qml:36
#, kde-format
msgid "Return to Clipboard"
msgstr "回到剪貼簿"

#: contents/ui/BarcodePage.qml:57
#, kde-format
msgid "QR Code"
msgstr "QR 碼"

#: contents/ui/BarcodePage.qml:58
#, kde-format
msgid "Data Matrix"
msgstr "資料矩陣"

#: contents/ui/BarcodePage.qml:59
#, kde-format
msgctxt "Aztec barcode"
msgid "Aztec"
msgstr "Aztec"

#: contents/ui/BarcodePage.qml:60
#, kde-format
msgid "Code 39"
msgstr "Code 39"

#: contents/ui/BarcodePage.qml:61
#, kde-format
msgid "Code 93"
msgstr "Code 93"

#: contents/ui/BarcodePage.qml:62
#, fuzzy, kde-format
#| msgid "Code 39"
msgid "Code 128"
msgstr "Code 39"

#: contents/ui/BarcodePage.qml:85
#, fuzzy, kde-format
#| msgid "Change the barcode type"
msgid "Change the QR code type"
msgstr "變更條碼型態"

#: contents/ui/BarcodePage.qml:114
#, fuzzy, kde-format
#| msgid "Creating barcode failed"
msgid "Creating QR code failed"
msgstr "建立條碼失敗"

#: contents/ui/BarcodePage.qml:123
#, kde-format
msgid "The QR code is too large to be displayed"
msgstr ""

#: contents/ui/clipboard.qml:26
#, kde-format
msgid "Clipboard Contents"
msgstr "剪貼簿內容"

#: contents/ui/clipboard.qml:27 contents/ui/Menu.qml:84
#, kde-format
msgid "Clipboard is empty"
msgstr "剪貼簿是空的"

#: contents/ui/clipboard.qml:55
#, fuzzy, kde-format
#| msgid "Configure Clipboard..."
msgid "Configure Clipboard…"
msgstr "設定剪貼簿..."

#: contents/ui/clipboard.qml:57
#, fuzzy, kde-format
#| msgid "Clear history"
msgid "Clear History"
msgstr "清除歷史"

#: contents/ui/DelegateToolButtons.qml:23
#, kde-format
msgid "Invoke action"
msgstr "請求活動"

#: contents/ui/DelegateToolButtons.qml:38
#, fuzzy, kde-format
#| msgid "Show barcode"
msgid "Show QR code"
msgstr "顯示條碼"

#: contents/ui/DelegateToolButtons.qml:54
#, kde-format
msgid "Edit contents"
msgstr "編輯內容"

#: contents/ui/DelegateToolButtons.qml:68
#, kde-format
msgid "Remove from history"
msgstr "從歷史紀錄中移除"

#: contents/ui/EditPage.qml:82
#, kde-format
msgctxt "@action:button"
msgid "Save"
msgstr ""

#: contents/ui/EditPage.qml:90
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr ""

#: contents/ui/Menu.qml:84
#, kde-format
msgid "No matches"
msgstr ""

#: contents/ui/UrlItemDelegate.qml:99
#, kde-format
msgctxt ""
"Indicator that there are more urls in the clipboard than previews shown"
msgid "+%1"
msgstr "+%1"

#~ msgid "Clear history"
#~ msgstr "清除歷史"

#, fuzzy
#~| msgid "Search..."
#~ msgid "Search…"
#~ msgstr "搜尋…"

#~ msgid "Clipboard history is empty."
#~ msgstr "剪貼簿歷史紀錄是空的"
